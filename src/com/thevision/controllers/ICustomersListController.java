package com.thevision.controllers;

import android.view.View;
import android.widget.AdapterView;

public interface ICustomersListController {

	public void onListItemClick(AdapterView<?> parent, View view, int position,long id);
	
}
