package com.thevision.controllers;

public interface IDashBoardController {

	public void onNewSaleClick();
	public void onPendingSaleClick();
}
