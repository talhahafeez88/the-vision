package com.thevision.controllers;

import java.util.ArrayList;

import com.thevision.R;
import com.thevision.db.CustomerInfoTable;
import com.thevision.dtos.CustomerInfo;
import com.thevision.presenters.CustomersAdapter;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class TheVisionController implements OnClickListener,OnItemClickListener {
	private static TheVisionController controller;

//	All listners goes  here
	private IDashBoardController dashBoardController;
	private ICategoriesController categoriesController;
	private ICustomersListController customersListController;
	
	/***
	 * Handler for customer table , user for insert,update,delete customer table
	 */
	private CustomerInfoTable customerInfoTable;

	private TheVisionController() {
	}

	public static TheVisionController getInstance() {

		return controller = controller == null ? new TheVisionController(): controller;

	}

	public void setDashBoardController(View view,IDashBoardController dashBoardController) {

		view.findViewById(R.id.newSale).setOnClickListener(this);
		view.findViewById(R.id.pendingSale).setOnClickListener(this);
		
		this.dashBoardController = dashBoardController;

	}

	public void setCategoriesController(View view,ICategoriesController categoiesController) {

		view.findViewById(R.id.selectCustomerMerge).setOnClickListener(this);
		view.findViewById(R.id.selectFrameMerge).setOnClickListener(this);
		view.findViewById(R.id.selectOthersMerge).setOnClickListener(this);
		
		this.categoriesController = categoiesController;

	}

	public void setCustomersListController(Context contex, View view,ICustomersListController customersListController,ArrayList<CustomerInfo> customers) {

		((ListView)view.findViewById(R.id.customersList)).setOnItemClickListener(this);
		
		CustomersAdapter adapter=new CustomersAdapter(contex, customers);
		
		((ListView)view.findViewById(R.id.customersList)).setAdapter(adapter);
		this.customersListController=customersListController;

	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.newSale:
			dashBoardController.onNewSaleClick();
			break;
		case R.id.pendingSale:
			dashBoardController.onPendingSaleClick();
			break;
		case R.id.selectCustomerMerge:
			categoriesController.onSelectCustomerClick();
			break;
		case R.id.selectFrameMerge:
			categoriesController.onSelectFrameClick();
			break;
		case R.id.selectOthersMerge:
			categoriesController.onSelectOthersClick();
			break;

		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {		
		customersListController.onListItemClick(parent, view, position, id);
	}

}
