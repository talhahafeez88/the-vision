package com.thevision.controllers;

public interface ICategoriesController {

	public void onSelectCustomerClick();
	public void onSelectFrameClick();
	public void onSelectOthersClick();
}
