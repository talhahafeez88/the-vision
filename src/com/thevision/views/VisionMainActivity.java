package com.thevision.views;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import com.thevision.R;

public class VisionMainActivity extends BaseActivity {	
	private long splashTime=2000l;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_container);
		getSupportActionBar().hide();

		if (savedInstanceState == null) {
			setFragment(new SplashFragment());
			
			startTimer();
		}
	}

	public void setFragment(Fragment fragment) {
		((FrameLayout)findViewById(R.id.container)).removeAllViews();
		getSupportFragmentManager().beginTransaction()
				.add(R.id.container, fragment).commit();

		
	}
	
	private void startTimer(){
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setFragment(new DashBoardFragment());
			}
		}, splashTime);
	}
}
