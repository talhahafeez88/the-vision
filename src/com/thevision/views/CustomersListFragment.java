package com.thevision.views;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.thevision.R;
import com.thevision.commons.ExtraKeys;
import com.thevision.controllers.ICustomersListController;
import com.thevision.controllers.IDashBoardController;
import com.thevision.controllers.TheVisionController;
import com.thevision.dtos.CustomerInfo;
import com.thevision.presenters.CustomersAdapter;
/**
 * A placeholder fragment containing a simple view.
 */
public class CustomersListFragment extends Fragment implements ICustomersListController{

	private ArrayList<CustomerInfo> customers;
	
	public CustomersListFragment() {
	}

	private void intitList(){
		customers=new ArrayList<CustomerInfo>();
		CustomerInfo custoInfo=new CustomerInfo(1, "Name1", "Date1", "OtherInfo1", "Address1");
		customers.add(custoInfo);		
		custoInfo=new CustomerInfo(2, "Name2", "Date2", "OtherInfo2", "Address1");
		customers.add(custoInfo);		
		custoInfo=new CustomerInfo(3, "Name3", "Date3", "OtherInfo3", "Address1");
		customers.add(custoInfo);		
		custoInfo=new CustomerInfo(4, "Name4", "Date4", "OtherInfo4", "Address1");
		customers.add(custoInfo);		
		custoInfo=new CustomerInfo(5, "Name5", "Date5", "OtherInfo5", "Address1");
		customers.add(custoInfo);		
		custoInfo=new CustomerInfo(6, "Name6", "Date6", "OtherInfo6", "Address1");
		customers.add(custoInfo);		
		custoInfo=new CustomerInfo(7, "Name7", "Date7", "OtherInfo7", "Address1");
		customers.add(custoInfo);
		custoInfo=new CustomerInfo(8, "Name8", "Date8", "OtherInfo8", "Address1");
		customers.add(custoInfo);		
		custoInfo=new CustomerInfo(9, "Name9", "Date9", "OtherInfo9", "Address1");
		customers.add(custoInfo);		
		custoInfo=new CustomerInfo(10, "Name10", "Date10", "OtherInfo10", "Address1");
		customers.add(custoInfo);		
		custoInfo=new CustomerInfo(11, "Name11", "Date11", "OtherInfo11", "Address1");
		customers.add(custoInfo);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_customers_list,
				container, false);
		intitList();
		TheVisionController.getInstance().setCustomersListController(getActivity(),rootView, this,customers);
		
		return rootView;
	}

	@Override
	public void onListItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		CustomerInfo customerInfo=(CustomerInfo)((CustomersAdapter)parent.getAdapter()).getItem(position);
		CustomerDetailFragment cDetail=new CustomerDetailFragment();
		Bundle args=new Bundle();
		args.putParcelable(ExtraKeys.CustomersListExtraKeys.CUSTOMER_INFO, customerInfo);
		
		cDetail.setArguments(args);
		
		((VisionMainActivity)getActivity()).setFragment(cDetail);
	}
}
