package com.thevision.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.thevision.R;
import com.thevision.controllers.ICategoriesController;
import com.thevision.controllers.TheVisionController;
/**
 * A placeholder fragment containing a simple view.
 */
public class CategoriesFragment extends Fragment implements ICategoriesController{

	public CategoriesFragment() {
	}

	private void initView(View view){
		((TextView)view.findViewById(R.id.selectFrameMerge).findViewById(R.id.categoryText)).setText("Select Frame");
		((Button)view.findViewById(R.id.selectFrameMerge).findViewById(R.id.categoryIcon)).setBackgroundResource(R.drawable.icn_frame);
		
		((TextView)view.findViewById(R.id.selectOthersMerge).findViewById(R.id.categoryText)).setText("Others");
		((Button)view.findViewById(R.id.selectOthersMerge).findViewById(R.id.categoryIcon)).setBackgroundResource(R.drawable.bg_others);

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_categories,
				container, false);
		initView(rootView);
		TheVisionController.getInstance().setCategoriesController(rootView, this);
		return rootView;
	}

	@Override
	public void onSelectCustomerClick() {
		// TODO Auto-generated method stub
		((VisionMainActivity)getActivity()).setFragment(new CustomersListFragment());
	}

	@Override
	public void onSelectFrameClick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSelectOthersClick() {
		// TODO Auto-generated method stub
		
	}

}
