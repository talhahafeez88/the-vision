package com.thevision.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thevision.R;
import com.thevision.commons.ExtraKeys;
import com.thevision.dtos.CustomerInfo;
/**
 * A placeholder fragment containing a simple view.
 */
public class CustomerDetailFragment extends Fragment {
	private CustomerInfo customerInfo;
	public CustomerDetailFragment() {
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.customerInfo=getArguments().getParcelable(ExtraKeys.CustomersListExtraKeys.CUSTOMER_INFO);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_customers_detail,
				container, false);
		View customerDetail=rootView.findViewById(R.id.customerDetail);
		
		
		((TextView)customerDetail.findViewById(R.id.serialNo)).setText(customerInfo.getSerialNo()+"");
		((TextView)customerDetail.findViewById(R.id.name)).setText(customerInfo.getName());
		((TextView)customerDetail.findViewById(R.id.date)).setText(customerInfo.getDate());
		((TextView)customerDetail.findViewById(R.id.otherInfo)).setText(customerInfo.getOtherInfo());
		((TextView)customerDetail.findViewById(R.id.address)).setText(customerInfo.getAddress());

		
		return rootView;
	}
}
