package com.thevision.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thevision.R;
import com.thevision.controllers.IDashBoardController;
import com.thevision.controllers.TheVisionController;
/**
 * A placeholder fragment containing a simple view.
 */
public class DashBoardFragment extends Fragment implements IDashBoardController{

	public DashBoardFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_dashboard,
				container, false);
		TheVisionController.getInstance().setDashBoardController(rootView, this);
		
		return rootView;
	}

	@Override
	public void onNewSaleClick() {
		((VisionMainActivity)getActivity()).setFragment(new CategoriesFragment());
	}

	@Override
	public void onPendingSaleClick() {
		
	}
}
