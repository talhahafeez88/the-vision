package com.thevision.commons;

public class DBTables {

	public static class CustomerTableInfo{
		public static final String TABLE_NAME="CustomerInfo";
		
		public static final String SERIAL_NO="SerialNo";
		public static final String NAME="Name";
		public static final String DATE="Date";
		public static final String ADDRESS="Address";
		public static final String OTHER_INFO="OtherInfo";
		
		
		public static final String CREATE_TABLE=" CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
	            SERIAL_NO + " INTEGER NOT NULL, " +
	            NAME + " TEXT NOT NULL, " +
	            DATE + " TEXT NOT NULL, " +
	            ADDRESS + " TEXT NOT NULL, " +
	            OTHER_INFO + " INTEGER NOT NULL, " +");";

	}
}
