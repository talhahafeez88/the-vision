package com.thevision.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.thevision.commons.DBTables;
import com.thevision.dtos.CustomerInfo;

public class CustomerInfoTable {
	private DBHelper dbHelper;
	
	public CustomerInfoTable(Context context){
		dbHelper=new DBHelper(context);
	}
	
	public Cursor getAllCustomerInfo(CustomerInfo customerInfo){
		SQLiteDatabase database=dbHelper.openDB();
		Cursor cursor= database.query(DBTables.CustomerTableInfo.TABLE_NAME, null, null, null, null, null, null);
		database.close();
		return cursor;
	}

	
	public long insertCustomerInfo(CustomerInfo customerInfo){
		ContentValues row=new ContentValues();
		row.put(DBTables.CustomerTableInfo.SERIAL_NO, customerInfo.getSerialNo());
		row.put(DBTables.CustomerTableInfo.NAME, customerInfo.getName());
		row.put(DBTables.CustomerTableInfo.DATE, customerInfo.getDate());
		row.put(DBTables.CustomerTableInfo.ADDRESS, customerInfo.getAddress());
		row.put(DBTables.CustomerTableInfo.OTHER_INFO, customerInfo.getOtherInfo());
		
		SQLiteDatabase database=dbHelper.openDB();
		long rowId=database.insert(DBTables.CustomerTableInfo.TABLE_NAME, null, row);
		database.close();
		return rowId;
	}

	public int updateCustomerInfo(CustomerInfo customerInfo){
		ContentValues row=new ContentValues();
		row.put(DBTables.CustomerTableInfo.SERIAL_NO, customerInfo.getSerialNo());
		row.put(DBTables.CustomerTableInfo.NAME, customerInfo.getName());
		row.put(DBTables.CustomerTableInfo.DATE, customerInfo.getDate());
		row.put(DBTables.CustomerTableInfo.ADDRESS, customerInfo.getAddress());
		row.put(DBTables.CustomerTableInfo.OTHER_INFO, customerInfo.getOtherInfo());
		
		SQLiteDatabase database=dbHelper.openDB();
		int noOfRows=database.update(DBTables.CustomerTableInfo.TABLE_NAME, row, "WHERE "+DBTables.CustomerTableInfo.SERIAL_NO+" =?", new String[]{String.valueOf(customerInfo.getSerialNo())});
		
		database.close();
		return noOfRows;
	}

	public int deleteCustomerInfo(CustomerInfo customerInfo){
		SQLiteDatabase database=dbHelper.openDB();
		int noOfRows=database.delete(DBTables.CustomerTableInfo.TABLE_NAME, "WHERE "+DBTables.CustomerTableInfo.SERIAL_NO+" =?", new String[]{String.valueOf(customerInfo.getSerialNo())});
		database.close();
		return noOfRows;
		
	}

}
