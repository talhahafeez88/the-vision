package com.thevision.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.thevision.commons.DBTables;

public class DBHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "Vision";
	private static final int DB_VERSION = 1;
	private SQLiteDatabase database;
		
	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DBTables.CustomerTableInfo.CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public SQLiteDatabase openDB() {
		if (database == null) {
			database = getWritableDatabase();
		}

		return database;
	}

	public void close() {
		if (database != null) {
			database.close();
		}
	}
}
