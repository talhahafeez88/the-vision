package com.thevision.dtos;

import android.os.Parcel;
import android.os.Parcelable;

public class CustomerInfo implements Parcelable{

	private int serialNo;
	private String name;
	private String date;
	private String otherInfo;
	private String address;
	
	public CustomerInfo(){}
	
	public CustomerInfo(Parcel source){
		serialNo=source.readInt();
		name=source.readString();
		date=source.readString();
		otherInfo=source.readString();
		address=source.readString();
	}
	
	public CustomerInfo(int serialNo,String name,String date,String otherInfo,String address){
		this.serialNo=serialNo;
		this.name=name;
		this.date=date;
		this.otherInfo=otherInfo;
		this.address=address;
	}
	
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getOtherInfo() {
		return otherInfo;
	}
	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeInt(serialNo);
		dest.writeString(name);
		dest.writeString(date);
		dest.writeString(otherInfo);
		dest.writeString(address);
	}
	
	public static final Parcelable.Creator<CustomerInfo> CREATOR=new Creator<CustomerInfo>() {
		
		@Override
		public CustomerInfo[] newArray(int size) {
			// TODO Auto-generated method stub
			return new CustomerInfo[size];
		}
		
		@Override
		public CustomerInfo createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new CustomerInfo(source);
		}
	};
}
