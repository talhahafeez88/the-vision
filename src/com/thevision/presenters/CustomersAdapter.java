package com.thevision.presenters;

import java.util.ArrayList;

import com.thevision.R;
import com.thevision.dtos.CustomerInfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustomersAdapter extends BaseAdapter{

	private Context context;
	private ArrayList<CustomerInfo> customers;
	private LayoutInflater inflater;
	
	public CustomersAdapter(Context context,ArrayList<CustomerInfo> customers){
		this.context=context;
		this.customers=customers;
		this.inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return customers.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return customers.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			convertView=inflater.inflate(R.layout.customer_info_merge, null);
		}
		
		CustomerInfo customer=customers.get(position);
		((TextView)convertView.findViewById(R.id.serialNo)).setText(customer.getSerialNo()+"");
		((TextView)convertView.findViewById(R.id.name)).setText(customer.getName());
		((TextView)convertView.findViewById(R.id.date)).setText(customer.getDate());
		((TextView)convertView.findViewById(R.id.otherInfo)).setText(customer.getOtherInfo());
		((TextView)convertView.findViewById(R.id.address)).setText(customer.getAddress());
		return convertView;
	}

}
